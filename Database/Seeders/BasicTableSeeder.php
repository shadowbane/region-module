<?php

namespace Modules\Region\Database\Seeders;

use Facades\Modules\Manager\Repositories\ModuleRepositories;
use Illuminate\Database\Seeder;

class BasicTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ModuleRepositories::getAndUpdate();
        if (config('region.use_laravolt_seeders')) {
            \Artisan::call("laravolt:indonesia:seed");
        }
    }
}
