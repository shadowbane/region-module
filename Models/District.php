<?php

namespace Modules\Region\Models;

use Backpack\CRUD\CrudTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class District extends \Laravolt\Indonesia\Models\District
{
    use CrudTrait, LogsActivity;
}
