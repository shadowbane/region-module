<?php

namespace Modules\Region\Models;

use Backpack\CRUD\CrudTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class City extends \Laravolt\Indonesia\Models\City
{
    use CrudTrait, LogsActivity;
}
