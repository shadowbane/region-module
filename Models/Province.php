<?php

namespace Modules\Region\Models;

use Backpack\CRUD\CrudTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class Province extends \Laravolt\Indonesia\Models\Province
{
    use CrudTrait, LogsActivity;
}
