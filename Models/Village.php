<?php

namespace Modules\Region\Models;

use Backpack\CRUD\CrudTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class Village extends \Laravolt\Indonesia\Models\Village
{
    use CrudTrait, LogsActivity;
}
