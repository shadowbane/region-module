<?php

namespace Modules\Region;

use Symfony\Component\Console\Output\ConsoleOutput;

class Region
{
    protected $console;

    public function __construct()
    {
        $this->console = new ConsoleOutput();
    }

    /**
     * Enabling Module Region
     * @return boolean
     */
    public function enable()
    {
        // enable the module
        \Artisan::call("module:enable", [
            'module' => 'Region'
        ]);

        // run migration
        \Artisan::call(
            "module:migrate-refresh",
            [
                'module' => 'Region'
            ]
        );

        // run seeder
        \Artisan::call("module:seed", [
            'module' => 'Region'
        ]);

        return true;
    }

    /**
     * Disabling Module Region
     * @return boolean
     */
    public function disable()
    {
        \Artisan::call("module:migrate-reset", [
            'module' => 'Region'
        ]);

        \Artisan::call("module:disable", [
            'module' => 'Region'
        ]);
        return true;
    }
}
